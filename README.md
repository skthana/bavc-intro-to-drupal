# Introduction to Drupal
A hands-on workshop on site-building using the latest Drupal release. 

# Course Outline
This outline is a roadmap for a two-day workshop at BAVC.  Unlike a slide deck, it is a resource you can return to, with links to the most current Drupal documentation.

---
# Day 1
**Objectives**

- Become familiar with the UI and capabilities of a basic Drupal 8 site
- Learn common Drupal terminology and functionality
- Practice basic site configuration and content creation
- Extend and customize Drupal 8

-----------------------
> ### Exercise 1 - Set Up a Drupal 8 Development Site
> 1. Navigate to Pantheon.io (https://pantheon.io/)
> 1. Set up a free account
> 1. Create a new __Drupal 8__ site
> 1. Once you're on Pantheon's dashboard, click "Visit Development Site" at the upper left.
> 1. Step through the entire intallation process (set a temporary name, choose your time zone, etc.)
> 1. When all steps have been completed, you should be on your new website's home page, with a welcome message in the content area.
> 1. Click through each of the admin menu's links and subsections.  What's the function of each?
-----------------------

## Some background
- [What is Drupal?](https://www.drupal.org/about)
- [User Guide](https://www.drupal.org/docs/user_guide/en/index.html)
- [Understanding Drupal](https://www.drupal.org/docs/user_guide/en/understanding-drupal.html)  
  [with notes](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/understanding-drupal.html)
- [The Drupal Project](https://www.drupal.org/docs/user_guide/en/understanding-project.html)  
  [with notes](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/understanding-project.html)

## Unboxing your local Drupal site
- [Anonymous & authenticated](https://www.drupal.org/docs/user_guide/en/user-concept.html) - what you see depends on who you are  
  [with notes](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/user-concept.html)
- [The administrative menu and contextual links](https://www.drupal.org/docs/user_guide/en/config-overview.html)  
  [with notes](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/config-overview.html)
- Using URL fragments to indicate specific admin locations
- [Site Configuration](https://www.drupal.org/docs/user_guide/en/config-basic.html) options - site name, slogan, contact email

-----------------------
> ### Exercise 2 - Create and edit an Article
> 1. Navigate to \> Content \> Add content
> 1. Select "Article"
> 1. Fill in all the fields of the input form
> 1. Save & take a look at your completed Article
> 1. Take a look at your website's home page & note the change
> 1. Next, try editing your new Article. Add a paragraph to the description, then save again.
> 1. Now, add two additional new Articles in the same way
> 1. Check the home page again.  What's changed?  In what order do the posts appear?  How might you control what appears on the home page?
-----------------------

## Scenario
# Build a website for your city's local library 
  :books:

-----------------------
> ### Exercise 3 - Create an "About" page and link to it from the Main Menu
> 1. Navigate to \> Content \> Add content
> 1. Select "Basic Page"
> 1. In the Title field, type "About the City Library" (or something else suitable)
> 1. Fill in the Body field with a few paragraphs of dummy text
> 1. Save & take a look at your Page
> 1. Take a look at your website's home page & note any change. Can you explain your observations?  If you wanted to edit your About page, how would you do that?
> 1. Next, add your new About page to the Main Menu. Navigate to \> Content to see a list of content items.
> 1. On the row listing your About page, click "Edit" on the far right side
> 1. Scroll to the bottom of the input form and select "Menus"
> 1. Check "Add link to Menu" and select the Main Menu from the puldown
> 1. Save and check if your new About page is linked from the Main menu
-----------------------

## Menus
- [Menus in Drupal](https://www.drupal.org/docs/user_guide/en/menu-concept.html) - several menus are pre-installed in a standard Drupal 8 site.
- Add, remove, re-order, enable & disable links in a menu
- The [Blocks](https://www.drupal.org/docs/user_guide/en/block-concept.html) admin UI can be used to place Menus where you want them
- It's possible to create additional Menus with a custom set of links

## Themes
- [Themes](https://www.drupal.org/docs/user_guide/en/understanding-themes.html)
  [with notes](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/understanding-themes.html)
- Themes comtrol the appearance of your site
- Presentation separated from content
- Only 8.x themes can be installed on Drupal 8 sites; 7.x themes on Drupal 7 sites, etc.

-----------------------
> ### Exercise 4 - Modify your website's appearance
> 1. Navigate to \> Admin \> Appearance
> 1. Which theme is enabled?  Which other themes are installed but disabled?
> 1. We can easily change the settings to re-color the current theme.
> 1. To the right of Bartik, click "Settings"
> 1. Use the options under Color to change the way your site looks.  The preview below will update so you can see what your new settings will look like.
> 1. Click "Save configuration"
> 1. Duplicate your browser tab or window so that you can edit in one and view in the other
> 1. In the new tab or window, navigate to your site's homepage and view your changes
-----------------------

-----------------------
> ### Exercise 5 - Install a new theme and set it as the default
> 1. In a new browser tab or window, navigate to [Drupal.org](http://drupal.org) (a.k.a "D-dot-O")
> 1. In the main nav, click "Download & Extend"
> 1. Scroll to the bottom, to the Themes section, second column, and click "More Most Installed"
> 1. Scroll down through the themes and click "ZURB Foundation"
> 1. For reference, that's [here](https://www.drupal.org/project/zurb_foundation)
> 1. On the Foundation project page, scroll down to the "Downloads" section
> 1. Right-click (or CMD-click) on the tar.gz link for the most recent release (8.x-6.0-alpha3), and copy the link URL
> 1. Switch back to the tab or window with your site admin UI, and if not still there, navigate to \> Admin \> Appearance
> 1. At the top, click the blue "Install a new theme" button
> 1. Paste the URL you copied for Foundation's tar.gz into the "Install from a URL" field and click "Install"
> 1. Once you see the theme listed on this page (you may need to reload the page), enable it and set as default
> 1. Switch to the tab with your site's homepage, refresh, and note the changes. Click around to explore.
> 1. Back in the Appearance tab, explore what settings this new theme allows you to confgure.
> 1. Further reading on [theme installation](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/extend-theme-install.html)
-----------------------

## Modules
- [Modules](https://www.drupal.org/docs/user_guide/en/understanding-modules.html)
  with notes: https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/understanding-modules.html
- Core modules = Modules that are part of the standard Drupal download.  Each of these provides an essential or at least commonly used functionality.
- Contributed ("Contrib") modules = Modules that are hosted on D.O as official projects, and can be installed if an additional feature is desired
  - Some contrib modules are known as "supermodules". They tend to provide powerful systems of functionality, above and beyond a simple feature.
- Custom modules = Modules that are created to store a site-specific functionality or feature. They are not usually generalized enough to be installed on any site other than the one they were created for.
- Each module installed contributes to page rendering times; best practice to uninstall any contrib modules not in use.
- As with themes, only 8.x modules can be installed on Drupal 8 sites.

-----------------------
> ### Exercise 6 - Install a contrib module: Admin Toolbar
> 1. In a new browser tab, navigate to the Admin Toolbar project page on D.O (https://www.drupal.org/project/admin_toolbar), scroll to the Downloads section at the bottom of the page.
> 1. Scroll to the Downloads section and copy the URL of the tar.gz link. This can be done by right-clicking (CMD-click) and selecting Copy link address.
> 1. Switch back to the tab with your site's admin UI open
> 1. Navigate to \> Extend
> 1. Click the blue "Install new module" button, then paste your copied URL into the into the "Install from a URL" field and click "Install"
> 1. Click "Enable newly added modules" to return to the Extend page, where you can locate and check the box next to the Admin Toolbar module
> 1. Click "Install" to turn on the new module
> 1. Confirm that the new Admin Toolbar module is enabled by navigating through the Admin menu at the top of your site
> 1. What's changed? How would you go about disabling / uninstalling a module? Which style admin menu do you find easier to work with?
>
> [Procedure on D.O](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/extend-module-install.html)
-----------------------

## Content Types
- A closer look at our Article and Basic Page content types
  - Fields, settings, form display, display modes
- Why create new custom Content Types?  
  - We already have Article and Basic Page. Why do we need more?
- [Modular Content](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/planning-modular.html)
- You're creating content, and you want to make it as useful as possible.
- Structure your content in a modular way - eg. store the data about each book in separate designated fields:
  Author(s), Publication date, Edition, Publisher, Category, Part of a Series, Cover image, etc.
- Using structured Content Types means you can do much more with your data
[Further reading](https://www.drupal.org/docs/user_guide/en/content-structure-chapter.html)
---
---
---
# Day 2
**Objectives**

- Become familiar with entities, entity references, regions, blocks, and views
- Gain a general understanding of Drupal 7 and how it differs from Drupal 8
- Work briefly with Backdrop (a Drupal 7 fork) and understand what it offers
- Become familiar with Drupal 8 taxonomy, users, permissions and roles

---
#### Day 1 Recap
- Created a development Drupal 8 website on the Pantheon platform
- Orientation of the D8 admin interface
- Created Article and Basic Page content
- Customized the Main Nav Menu
- Installed and enabled a new theme
- Extended site functionality by installing and working with contrib modules
- Created a new custom Content Type and began working with a few field types
- Introduced entity references

-----------------------
> ### Exercise 7 - Create a custom Content Type: Book, and add a Publication Date field
> __Part A__

> 1. Navigate to \> Structure \> Content types
> 1. Click the blue "Add content type" button
> 1. Fill in the name (eg. "Book") and description (eg. "Use *book* to enter data about a single book")
> 1. Below you'll find the vertical tabs. What do each of these options do, and what configuration of these makes the most sense for our "Book" content type?
> 1. Click "Save and manage fields"
> 1. Add a field for Publication Date: click the blue "Add field" button
> 1. Add a new field of type "Date"
> 1. How should the Pub Date field be configured? Do a minute or so of research to see how pub dates are usually listed, and then do your best to configure your date field appropriately.
> 1. Might there be a better way?
>
> __Part B__

> 1. Install contrib module "Year Only" https://www.drupal.org/project/yearonly
> 1. Now try again to set up a Pub Date field, this time selecting the new Year Only field type
> 1. What alternate solutions could be used for this situation?
>
> Additional reading:
> [Entities and Fields](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/planning-data-types.html)
> [Adding Fields to a Content Type](https://www.drupal.org/docs/user_guide/en/structure-fields.html)
-----------------------

-----------------------
> ### Exercise 8 - Customize the settings and displays for your new content type
> 1. On the Edit tab of your Book content type, adjust the settings so that content is published by default, not promoted to the front page, not sticky, and revisions are created.
> 1. Adjust settings so that the author and date are not displayed
> 1. Next, adjust the form display settings.  How can you make the admin UI easier to work with for your content editors?
-----------------------

## Field types
- [Fields that come with Drupal core](https://www.drupal.org/docs/8/api/entity-api/fieldtypes-fieldwidgets-and-fieldformatters)
- Choosing the right field type
  - Provides functionality
  - Built-in data validation
  - __Significance__ of your data
- Add field types via [contrib modules](https://www.drupal.org/search/site/field?f%5B0%5D=ss_meta_type%3Amodule)

-----------------------
> ### Exercise 9 - Add more fields to the Book content type
>
> __Part A__
>
> | Field name | Field type | Notes |
> |------------|------------|-------|
> | ISBN       | Plain text | Why not a number field? |
> | Cover Image | Image     | Is there a field you can re-use? |
> | Author Name | Plain text | How would multiple authors be added? |
> 
> 1. Add the three fields above.
> 1. Think about the Author Name field. How could you make it more useful?  What if an author has multiple books at this library? What would prevent your editors from misspelling the author's name on one of their books?
> 1. We want to do more with Author(s): provide a bio and photo, list all their books automatically, prevent human error
> 1. To make this possible, we create another content type: __Author__
>
> __Part B__
> 
> 1. Create a new content type called "Author" and customize its display settings
> 1. Save and manage fields. Edit the Body fielt so that it's named "Bio"
> 1. Add two new plain text fields: First Name, Last Name
> 1. Add the already-configured Image field, and give it a label of "Headshot"
> 1. Save and test by adding an Author node.  What do you notice?
> 1. To programatically build a Title that pulls the first and last names, we need some more contrib modules.
>
> __Part C__

> 1. Locate and install two modules: Token, Automatic Entity Label 
> 1. Return to your new Author content type and switch to the new tab that's appeared, "Automatic Label"
> 1. Place your cursor in the large textarea field
> 1. Below the textarea, click the link to "Browse available tokens"
> 1. In the modal that appears, expand the "Node" category and locate "First Name". Click the linked token next to that entry.
> 1. Repeat for "Last Name", then close the modal. What's changed?
> 1. Save, and test again by adding another Author. Did you achieve the desired behavior?  Any adjustments needed?
> 
> Now, we can go back to our Book content type and add a field that lets us select an author from the ones we've added as nodes of type Author.
>
> __Part D__

> 1. In the Book content type, add a new field of type Reference \> Content, and name it Author. Note that the label can be whatever you like, but the machine name must be unique.
> 1. Be sure to add help text for this field that lets the editor know how to add a new Author node. You can even provide a link to the "add Author" form!
> 1. Restrict the content that can be referenced to the Author content type
> 1. Save, and test by adding a Book.  Note the difference in the way the two Author fields work.  
> 1. Remove the original Author Name field and save
> 1. How could content structure planning up-front have made this process smoother? 
>
> __Bonus__

> 1. Revisit the [example planning spreadsheet](https://docs.google.com/spreadsheets/d/15htLLWLguhwiuTLg_nndQNpgWVdUMy6UaR_d1q-v6iw/edit#gid=0) and download or save to your own Drive account so that you can edit it.
> 1. Use the "Node types" and "Fields" tabs to map what you've just done in Drupal.  So, list the fields you've created, then add the node types you've set up
-----------------------

## Entities
- [Entities and Fields](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/planning-data-types.html)
- Entities can come in various forms.  Content types are one kind of entity; Taxonomy terms are another type.
- An abstraction and kind of a difficult term, but important to know what others are referring to

## View modes
- [View Modes and Formatters](https://www.drupal.org/docs/user_guide/en/structure-view-modes.html)
- Drupal makes it easy to achieve consistent presentation of your content.
- You can format the same content to appear in multiple places in different ways
- There are some standard View Modes that come with Drupal core
- You can create additional View Modes to suit your needs

-----------------------
> ### Exercise 10 - Cusomize your Book page display
> 1. Adjust the display settings for your fields in the Default view mode. Where do you see this take effect?
> 1. Try enabling the "Full Content" View mode (hint: it's hidden under "Custom Display Settings")
> 1. Adjust the settings for the Full Content view mode. Where do these changes appear?
-----------------------

## Regions
- [Understanding regions](https://via.hypothes.is/https://www.drupal.org/docs/user_guide/en/block-regions.html)
- Every theme defines a set of regions
- Site content can be arranged in these regions using the Blocks Admin UI

## Blocks
- What are blocks and regions
- How to place, move, and disable blocks
- How to get blocks to appear only on selected pages
- Custom blocks

-----------------------
> ### Exercise 11 - Customize the layout of your site
> 1. Navigate to \> Structure \> Blocks
> 1. Click the "Demonstrate block regions" button
> 1. Place a new block in one of the sidebar regions
> 1. Save and view changes
-----------------------

## Views and views displays
- Exploring the views interface
- Existing views you're already using, and how they were built
- Duplicating and customizing views
- Views displays

-----------------------
> ### Exercise 12 - Create a "Newest Books" view with a block display, then place that block in the sidebar of your site
> 1. Navigate to \> Structure \> Views
> 1. Use the wizard to create a new View called Newest Books
> 1. Show content of type Book, display Teasers, and create a Page and Block display
> 1. Save and take a look at your new view. Find the link to view your page display and observe the result
-----------------------
-----------------------
> ### Exercise 13 - Create a custom View Mode for the Book content type, then create another block display in your Newest Books view that uses this View Mode
> 1. Add a new Content View Mode by navigating to \> Structure \> Display Modes \> View Modes \> Add new Content View Mode
> 1. Name it "Compact"
> 1. Set your display so that only the title and image are shown, then save
> 1. In your Newest Books view, clone your Block display and name it Compact Block
> 1. Adjust the content to display "Compact" instead of "Teaser"
> 1. Scroll down to see the preview, and make any adjustments.  Save
> 1. Under \> Structure \> Blocks, install your new block, and place it in the First Sidebar region
-----------------------

## Drupal 7 Overview
- Chances are you will only work with existing D7 sites, not install a new one.  
- A handy way to just click around a Drupal site: [SimplyTest.me](https://simplytest.me/)

## Backdrop
[backdropcms.org](https://backdropcms.org/)

## Taxonomy, vocabularies, and terms
## Users, roles, and permissions

## More links & stuff to read
- [Things that were new in Drupal 8](https://dev.acquia.com/blog/10-new-features-in-drupal-8-core/18/05/2016/6721)
- [Drupal version numbers](https://www.drupal.org/docs/8/understanding-drupal-version-numbers)
- [System requirements](https://www.drupal.org/docs/8/system-requirements) and [installation](https://www.drupal.org/docs/8/install)
- [Community](https://www.drupal.org/community), [Drupal Camps and Drupalcons](https://www.drupal.org/events)
  - [BADCamp](https://2017.badcamp.net)
  - Drupal User Groups
  - IRC Channels
- [Hosting](https://www.drupal.org/hosting), [Try Drupal](https://www.drupal.org/try-drupal)

## Content modeling & planning examples: 
- [Palantir](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiG_-vNyfbQAhXmx1QKHemnCMUQFggpMAA&url=https%3A%2F%2Fwww.palantir.net%2Fblog%2Fdeveloping-drupal-sites-plan-or-perish&usg=AFQjCNFu61dPmWo_u8aASaWZo3AWuMArzQ&sig2=9fG_gQ1UdrlxAPgR7cG4Ww)
- [Aten](http://atendesigngroup.com/blog/creating-blueprints-drupal-content)
- [Diagram](https://www.gliffy.com/go/publish/3007867/)

## Random links:
- [Why is Drupal SO complicated?](http://www.farncom.be/blog/drupal-steep-cms-learning-curve)
- [Who runs Drupal?](https://drupalize.me/videos/drupal-community-demystified)
- [BADCamp 2017](https://2017.badcamp.net/free-drupal-training) - see you there!
- [Dummy text, the plain and the hysterical](https://www.designbombs.com/hysterical-lorem-ipsum-generators/)


